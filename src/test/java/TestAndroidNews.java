
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import page.PageMain;
import page.PageNews;
import util.Settings;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

public class TestAndroidNews extends Settings {

    private static PageMain pageMain;

    @BeforeClass
    public static void setUp () throws Exception{
        uploadApp();
        pageMain = new PageMain(driver);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }


    @Test
    public void test_detailed_description_news() throws Exception{

        PageNews pageNews = pageMain.openNews(3);

        String title = pageNews.getTitleNews().getText();

        assertThat(title, is("В России появится миграционный кодекс"));

    }

    @Test
    public void test_infinity_scroll() throws Exception{

        String titleBeforeScroll = pageMain.getTitle();
        int i = 0;
        while (i < 10){
            driver.swipe(0,getHight() - 1,0,1,0);
            String titleLastScroll = pageMain.getTitle();
            if (titleBeforeScroll.equals(titleLastScroll))
                break;
            else
                titleBeforeScroll = titleLastScroll;
            i++;
        }
        Assert.assertTrue(i > 5);

    }

    @Test
    public void test_pagination() throws Exception{

        driver.swipe(0, getHight() - 1, 0, 1, 0);

        String titleLastScroll = pageMain.getTitle();

        driver.swipe(0, getHight() - 1, 0, 1, 0);

        assertThat(pageMain.getTitle(), not(titleLastScroll));
    }
}
