package util;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Settings {

    public static AndroidDriver driver;
    private static int hight;

    /**
     * Upload apk to device
     */
    protected static void uploadApp() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        // Set android deviceName desired capability. Set it Android Emulator.
        capabilities.setCapability("deviceName", "Android Emulator");
        capabilities.setCapability("platformVersion", "5.0");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("appPackage", "com.news.inkubator.news");
        capabilities.setCapability("appActivity", "com.news.inkubator.news.presentation.news.NewsActivity");


        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        hight = driver.manage().window().getSize().height;
    }

    public static int getHight() {
        return hight;
    }
}
