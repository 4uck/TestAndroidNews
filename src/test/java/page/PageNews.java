package page;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PageNews {

    AppiumDriver driver;

    private By titleNews;

    public PageNews(AppiumDriver driver) {
        this.driver = driver;
        this.titleNews = By.id("com.news.inkubator.news:id/tv_title");
    }

    public WebElement getTitleNews() {
        return driver.findElement(titleNews);
    }
}
