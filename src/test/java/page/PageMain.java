package page;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PageMain {

    AppiumDriver driver;

    private List<WebElement> news;

    private By title;

    public PageMain(AppiumDriver driver) {

        this.driver = driver;
        this.news = driver.findElements(By.id("com.news.inkubator.news:id/card_view"));
        this.title = By.id("com.news.inkubator.news:id/tv_recycler_item");
    }

    public PageNews openNews(int index){
        news.get(index).click();
        return new PageNews(driver);
    }

    public String getTitle(){
        return driver.findElement(title).getText();
    }
}
